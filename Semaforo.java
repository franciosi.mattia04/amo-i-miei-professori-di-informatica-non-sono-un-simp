public class Semaforo {
    String colore ="verde";
    int timer = 50, timerAttuale = 0;
    public  Semaforo(int tick) {
        timer = tick;
    }

    public int init(){


        while (true) {
            if (timerAttuale < 0) {
                change();
                System.out.println(toString());

                timerAttuale = timer;
            }
            timerAttuale--;
        }
    }
    public String toString(){
        return colore;
    }
    public void change(){
        switch (colore){
            case "verde":
                colore="giallo";
                break;
            case "giallo":
                colore="rosso";
                break;
            case "rosso":
                colore="verde";
                break;
        }
    }
}
